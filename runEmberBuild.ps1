##Starting 
$sanitizedBranchName= $args[0]
$Folder= $args[1]
$revisionNumber = $args[2]
$bitbucketURL = $args[3]
$buildResultKey = $args[4]

$workingDirectory = "D:\RemoteRunEmberFolders\"+$Folder+"\"+$buildResultKey

$jsonFileName = "package.json"

#### function definition 

## function expects BitbuketURL to be in with ssh protocol
function cloneRepo($BranchName, $BitbucketURL, $WorkingDirectory){
	git clone -b $BranchName $BitbucketURL $WorkingDirectory	
	exitWithMessage "Couldn't clone. Exiting out!! Please try again." "$LASTEXITCODE"	
}

## Call this function before running NPM builds, so that Registry server and user are set
function  setNPMEnv($ArtifactServer){	
    npm set registry $ArtifactServer
    npm ping --registry $ArtifactServer	
	exitWithMessage "Registry server is not reachable!!."  "$LASTEXITCODE"		
}

function cleanWorkingDirectory($Directory){
	if (  !(Test-Path $Directory ) ){
		Write-Host "Nothing to clean"
	}
	
	else{
		Get-ChildItem -Path $Directory  -Recurse | Remove-Item -force -recurse			
		exitWithMessage "Couldn't clean working directory. Exiting out!! Please try again." "$LASTEXITCODE"		
	}
}

function runBuild($Directory){	
	Set-Location $Directory
	yarn install;	ember test	
	exitWithMessage "Ember test failed!!."  "$LASTEXITCODE"		
}

function getPackageVersionOnServer($PackageName){	
	$packageVersion = $( npm view $PackageName version )
    return $packageVersion
}

## function accepts name of json file and name of key. With Powershell 2 , it is not easy to Parse Json objects. 
function getValueFromJson($FileName,$Key){
	Set-Location $workingDirectory
	$command = "require('./$FileName').$Key"	
	$value = $( node -p -e $command )	
	return $value
}

function isValidVersion($localPackageVersion,$remotePackageVersion){	
	Write-Host "$localPackageVersion $remotePackageVersion"  
	if ($localPackageVersion,$remotePackageVersion){
		Write-Host "Latest version on registry server and branch are equal!!!" 
		return $false
    }
	
	$localVersion = $localPackageVersion.Split('.')
	$remoteVersion = $remotePackageVersion.Split('.')
	Write-Host "$localVersion $remoteVersion"  
	do {
        if (($localVersion.length -le 0) -and ($remoteVersion -le 0)) {
            return $false
        }

        if ($localVersion.length -le 0) {
            return $false
        }

        if ($remoteVersion.length -le 0) {
            return $true
        }

        # we know there is a number
        Write-Host "comparing $localVersion[0] and $remoteVersion[0]"

		if ([int]$localVersion[0] -gt [int]$remoteVersion[0]) {			
            return $true
		}       
		if ([int]$localVersion[0] -lt [int]$remoteVersion[0]) {			
            return $false
		}

        # at this point both versions are still equal
        $localVersion = $localVersion[1..($localVersion.length-1)]
        $remoteVersion = $remoteVersion[1..($remoteVersion.length-1)]
    } while($true)
}

## Publish package from folder 
function publishAddon($Directory){	
	Set-Location $Directory	
	npm publish
}

## exit with message and clean working directory before exit
function exitWithMessage($message,$exitCode){		
		Write-Host "$message"
		cleanWorkingDirectory $workingDirectory
		exit $exitCode
}

### End funtion definition

Write-Host "Start from clean directory $workingDirectory"
cleanWorkingDirectory $workingDirectory

cloneRepo $sanitizedBranchName $bitbucketURL $workingDirectory

##make sure that you have right commit checked out
Set-Location $workingDirectory; git checkout $revisionNumber

$registryServer = getValueFromJson "$jsonFileName" "publishConfig.registry"

setNPMEnv $registryServer
runBuild $workingDirectory

#get package name from package.json file. it is expected to be at root of cloned repository
$packageName = getValueFromJson "$jsonFileName" "name"

## get version of package on local machine and registry server
$remotePackageVersion = getPackageVersionOnServer $packageName
$localPackageVersion = getValueFromJson "$jsonFileName" "version"

Write-Host "capturing $localPackageVersion $remotePackageVersion"
$ValidVersion =  isValidVersion $localPackageVersion $remotePackageVersion

if ( $ValidVersion ){
	Write-Host "Package version $localPackageVersion is good!!"
	if ( $sanitizedBranchName -eq "develop" ){
		Write-Host "Publish package on develop branch"
		publishAddon $workingDirectory 	
	}	
}
else{	
	exitWithMessage "Package version $localPackageVersion is behind!!" "1"
}

##clean-up after build
cleanWorkingDirectory $workingDirectory

